<? 
/**
 * Sendgrid implementation for notify by email
 *
 * @author elalecs
 * @author EnriqueVelardeG
 * @version 0.1
 *
 */

class notifications_Notifications {

	private static $instance;
	private $db;
	private $lang;
	private $config;

	public function __construct(){
		$this->db = DB::getInstance();
		$this->config = $GLOBALS['config'];
		
	}

	/**
	 * Singleton pattern http://en.wikipedia.org/wiki/Singleton_pattern
	 * @return object Class Instance
	 */

	public static function getInstance()
	{
		if (!self::$instance instanceof self)
			self::$instance = new self;

		return self::$instance;
	}

	public function create($id_user, $notification, $params = array(), $reply = false){

		if(is_array($id_user)){
			$subject = (object) $id_user;
		}else{
			$id_user = (int) $id_user;
			$subject = $this->db->queryRow("SELECT label, email FROM ac_users WHERE id_user = {$id_user} ");
		}		

		if(!isset($subject->email)){
			return false;
		}

		$notification = $this->db->queryRow("SELECT 
				* 
			FROM
				notifications 
			WHERE 
				seo_name = '{$notification}' 
			OR 
				id_notification = '{$notification}' 
			LIMIT 1 
		");

		if(!isset($notification->id_notification)){
			return false;
		}

		foreach ($params as $index => $value) {
			$notification->content = StringUtils::formated($notification->content, array(
	            $index => $value
		    ));
		}

		$resp = $this->sendGrid($subject,$notification,$reply);		

		return $resp;	
		
	}

	private function sendGrid($subject, $notification, $reply = false){
		
		require_once 'libs/notifications/unirest-php/lib/Unirest.php';
		require_once 'libs/notifications/sendgrid-php/lib/SendGrid.php';
		SendGrid::register_autoloader();

		$sendgrid = new SendGrid('stayin', '.Acceso2014');

		$mail = @ new SendGrid\Email();
	
		$mail->setFrom('noreply@stayin.mx')
		     ->setFromName('Servicios Stayin.mx');
		
		if($reply){
			$mail->addTo($subject->email, $subject->label)  
				->addTo('enrique@wt.com.mx')
			///	->addTo('dev@wt.com.mx')
			    ->setSubject($notification->name)			   
			    ->setHtml($notification->content);
		}else{
			$mail->addTo($subject->email, $subject->label)
			     ->setSubject($notification->name)
			     ->setHtml($notification->content);
		}
		
		$mail_status = @$sendgrid->web->send($mail);
		
		return $mail_status;
	}	


}