<?php
/**
 * Logs stayin
 *
 * @author EnriqueVelardeG (http://twitter.com/EnriqueVelardeG)
 * @version 0.3
 *
 */

class logs_Logs {

	private static $instance;

	private $db_logs;
	function __construct() {
		$this->db_logs = DB::getInstance(1);
	}

	/**
	 * Singleton pattern http://en.wikipedia.org/wiki/Singleton_pattern
	 * @return object Class Instance
	 */
	public static function getInstance()
	{
		if (!self::$instance instanceof self)
			self::$instance = new self;

		return self::$instance;
	}

	public function setLogWidget($data = array()){

		$default = array(
			'id_type' => 0,
			'id_hotel' => 0,
			'id_user' => 0,
			'id_destination' => 0,
			'key_widget' => '',
			'val' => '',
			'place' => '',
			'id_client' => 0,
		);
		$data  = $data  + $default;
		$session = session_id();

		if($data['id_type'] > 0){

			//existe logs_resume//
			$id_resume = (int) $this->db_logs->queryOne("
				SELECT
					id_resume
				FROM
					logs_resume
				WHERE
					created_at = CURRENT_DATE
				AND
					id_type = {$data['id_type']}
				AND
					key_widget = '{$data['key_widget']}'
				LIMIT 1
			");

			//Log resume nuevo/editar
			if($id_resume > 0){
				$this->db_logs->query("
					UPDATE 
						logs_resume 
					SET 
						updated_at = NOW(), 
						resume = resume + 1 
					WHERE 
						id_resume = {$id_resume}						
				");
			}else{
				$this->db_logs->query("
					INSERT INTO
						logs_resume
					SET
						id_type = {$data['id_type']},
						id_hotel = {$data['id_hotel']},
						key_widget = '{$data['key_widget']}',
						id_user = {$data['id_user']},
						resume = 1,
						created_at = CURRENT_DATE,
						updated_at = NOW()
				");
			}

			//Nuevo Log
			$this->db_logs->query("
				INSERT INTO
					logs
				SET
					id_type = {$data['id_type']},
					id_hotel = {$data['id_hotel']},
					key_widget = '{$data['key_widget']}',
					id_user = {$data['id_user']},
					id_destination = {$data['id_destination']},
					id_client = {$data['id_client']},
					val = '{$data['val']}',
					place = '{$data['place']}',
					session = '{$session}',
					created_at = NOW()
			");
			return true;
		}

		return false;
	}


	public function setLog($data = array()){

		$default = array(
			'id_type' => 0,
			'id_hotel' => 0,
			'id_user' => 0,
			'id_destination' => 0,
			'val' => '',
			'place' => '',
			'id_client' => 0,
		);
		$data  = $data  + $default;
		$session = session_id();

		if($data['id_type'] > 0){

			//existe logs_resume//
			$id_resume = (int) $this->db_logs->queryOne("
				SELECT
					id_resume
				FROM
					logs_resume
				WHERE
					created_at = CURRENT_DATE
				AND
					id_type = {$data['id_type']}
				AND
					id_hotel = {$data['id_hotel']}
				AND
					id_user = {$data['id_user']}
				LIMIT 1
			");

			//Log resume nuevo/editar
			if($id_resume > 0){
				$this->db_logs->query("
					UPDATE 
						logs_resume 
					SET 
						updated_at = NOW(), 
						resume = resume + 1 
					WHERE 
						id_resume = {$id_resume}");
			}else{
				$this->db_logs->query("
					INSERT INTO
						logs_resume
					SET
						id_type = {$data['id_type']},
						id_hotel = {$data['id_hotel']},
						id_user = {$data['id_user']},
						resume = 1,
						created_at = CURRENT_DATE,
						updated_at = NOW()
				");
			}

			//existe logs_resume_session//
			$id_resume_session = (int) $this->db_logs->queryOne("
				SELECT
					id_resume_session
				FROM
					logs_resume_session
				WHERE
					DATE_FORMAT(created_at, '%Y-%m-%d') = CURRENT_DATE
					AND
					id_type = {$data['id_type']}
					AND
					session = '{$session}'
				LIMIT 1
			");

			//Log resume nuevo/editar
			if($id_resume_session > 0){
				$this->db_logs->query("
					UPDATE 
						logs_resume_session
					SET
						resume = resume + 1,
						updated_at = NOW()
					WHERE
						id_resume_session = {$id_resume_session}
				");
			}else{
				$this->db_logs->query("
					INSERT INTO
						logs_resume_session
					SET
						id_type = {$data['id_type']},
						session = '{$session}',
						resume = 1,
						created_at = NOW(),
						updated_at = NOW()
				");
			}

			//Nuevo Log
			$this->db_logs->query("
				INSERT INTO
					logs
				SET
					id_type = {$data['id_type']},
					id_hotel = {$data['id_hotel']},
					id_user = {$data['id_user']},
					id_destination = {$data['id_destination']},
					id_client = {$data['id_client']},
					val = '{$data['val']}',
					place = '{$data['place']}',
					session = '{$session}',
					created_at = NOW()
			");
			return true;
		}

		return false;
	}

}