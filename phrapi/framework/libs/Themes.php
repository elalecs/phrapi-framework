<?php defined("PHRAPI") or die("Direct access not allowed!");
/**
 * Hace un parse de un request_uri y define las constantes:
 *   TEMPLATE_PATH - Path relativo donde se encuentra el template
 *   BASED_PATH - Path relativo donde se encuentra el template sobre el que se basa
 *   TEMPLATE_FILE_PATH - Path absoluto donde se encuentra el archivo al que se accede
 */
class Themes {
    private $config;
    private $request_raw;
    private $request;
    private $themes = [];

    function __construct($config) {
        // la configuración del sitio
        $this->config = $config;

        // solo en desarrollo, se expone el poder cambiar de plantilla por URL
        if (preg_match('~\.(test|xyz)$~', $_SERVER['SERVER_NAME'])) {
            // esta en get y tiene valor, entonces se guarda en sesion y se usa el valor
            if (isset($_GET['theme']) && !empty($_GET['theme'])) {
                $this->config["plantilla"] = $_SESSION['theme'] = $_GET['theme'];
            }
            // esta en get pero es vacio, entonces se quita de sesion
            elseif (isset($_GET['theme']) && empty($_GET['theme'])) {
                unset($_SESSION['theme']);
            }
            elseif (isset($_SESSION['theme']) && !empty($_SESSION['theme'])) {
                $this->config["plantilla"] = $_SESSION['theme'];
            }

            $GLOBALS['actual_theme'] = $this->config["plantilla"];
        }

        // parse a la URI
        $this->parse();

        // se valida que exista la plantilla
        $this->themes['default'] = $this->setTheme($this->config["plantilla"]);
       
        // si la plantilla esta basada en otra se valida la padre
        if (isset($this->themes['default']['config']['based_on']) && $this->config["plantilla"] !== $this->themes['default']['config']['based_on']) {
            $this->themes['parent'] = $this->setTheme($this->themes['default']['config']['based_on']);
        }

        // define las constantes
        $this->setConstants();
    }

    /**
     * En caso de que existan reglas alias o abreviaciones para acceder se procesan,
     * se busca dentro del archivo config.php un indice 'request_rules' que contenta
     * un arreglo donde cada indice es el patrón a buscar en el request_uri y el valor
     * es el reemplazo a aplicar a la URI:
     * 
     * $config = [
     *     …
     *     'request_rules' => [
     *         '^/(terminos-condiciones|privacidad|hotel)' => '/content.php?alias=$1',
     *         …
     *     ]
     *     …
     * ];
     */
    private function runRules($raw) {
        if (!isset($this->config['request_rules'])) {
            return $raw;
        }

        foreach($this->config['request_rules'] as $pattern => $replacement) {
            if (preg_match("~{$pattern}~", $raw)) {
                $raw = preg_replace("~{$pattern}~", $replacement, $raw);
                break;
            }
        }

        // En caso de que el reemplazo sea hacia algo en phrapi se hace un redirect
        // por que este index no tiene nada que hacer
        if (preg_match("~^/phrapi~", $raw)) {
            error_log("Redirect: {$raw}");
            header("Location: {$raw}");
        }

        return $raw;
    }

    /**
     * Obtiene de REQUEST_URI los datos necesarios y los almacena en la propiedad request
     * y los argumentos los agrega a _GET y a _REQUEST
     */
    private function parse() {
        $this->request_raw = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : "";
        $this->request_raw = $this->runRules($this->request_raw);
        $this->request = parse_url($this->request_raw);

        if (isset($this->request['query'])) {
            $_SERVER['QUERY_STRING'] = $this->request['query'];

            if (!empty($this->request['query'])) {
                parse_str($this->request['query'], $args_uri);
                $_GET = $args_uri + $_GET;
                $_REQUEST = $args_uri + $_REQUEST;
            }
        }

        if (empty($this->request['path']) || $this->request['path'] === "/") {
            $this->request['path'] = "index.php";
        }

        $this->request['path'] = ltrim($this->request['path'], "/");

        return true;
    }

    /**
     * Con base al argumento se valida que exista el template requerido y se
     * carga su configuración
     */
    private function setTheme($desired_theme) {
        $theme = [];

        $desired_theme = ltrim($desired_theme, "/");

        // cuando no tenga / al final se le pone
        if (stripos(strrev($desired_theme), "/") !== 0) {
            $desired_theme = "{$desired_theme}/";
        }

        $theme['path'] = "/themes/{$desired_theme}";
        
        if (!is_dir("{$_SERVER['DOCUMENT_ROOT']}{$theme['path']}")) {
            http_response_code(503);
            die("Theme doesn't exists");
        }

        $package_path = "{$_SERVER['DOCUMENT_ROOT']}{$theme['path']}package.json";
        if (!is_file($package_path) || !is_readable($package_path)) {
            http_response_code(503);
            die("Theme package doesn't exists");
        }

        // obtenemos la data del archivo packege.json
        $theme['config'] = json_decode(file_get_contents($package_path), !!"assoc_true");

        return $theme;
    }

    /**
     * Se valida si el recurso al que se intenta acceder existe en el template activo
     * o en el template en el que se basa
     */
    private function validate() {
        $values = [];

        $based_path = $theme_path = $this->themes['default']['path'];

        if (isset($this->themes['parent'])) {
            $based_path = $this->themes['parent']['path'];
        }

        $file_path = "{$_SERVER['DOCUMENT_ROOT']}{$theme_path}{$this->request['path']}";

        if(file_exists($file_path) && is_readable($file_path)) {
            return compact('theme_path', 'based_path', 'file_path');
        }

        if (isset($this->themes['parent'])) {
            $file_path = "{$_SERVER['DOCUMENT_ROOT']}{$based_path}{$this->request['path']}";

            if(file_exists($file_path) && is_readable($file_path)) {
                return compact('theme_path', 'based_path', 'file_path');
            }
        }

        $default_path = "/themes/default/";
        return [
            'theme_path' => $default_path,
            'based_path' => $default_path,
            'file_path' => "{$_SERVER['DOCUMENT_ROOT']}$default_path/404.php"
        ];
    }

    /**
     * Se definen las constantes de los paths activos para el recurso al que se
     * accede
     */
    private function setConstants() {
        $values = $this->validate();

        // asignamos las variables a las constantes
        define("TEMPLATE_PATH", $values['theme_path'] );
        define("BASED_PATH", $values['based_path'] );
        define("TEMPLATE_FILE_PATH", $values['file_path']);
    }

    /**
     * Devuelve el path del archivo que se intenta cargar
     */
    public function getLoadPath($resource) {
        $try_file = $_SERVER['DOCUMENT_ROOT'].TEMPLATE_PATH."$resource";
        if (file_exists($try_file) && is_readable($try_file)) {
            return $try_file;
        }

        $try_file = $_SERVER['DOCUMENT_ROOT'].BASED_PATH."$resource";
        if (file_exists($try_file) && is_readable($try_file)) {
            return $try_file;
        }

        return false;
    }
}