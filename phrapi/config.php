<?php defined("PHRAPI") or die("Direct access not allowed!");

$config = [
    'gmt' => '-05:00',
    'locale' => 'es_MX',
    'php_locale' => 'es_MX',
    'timezone' => 'America/Mexico_City',
    'gmaps_key' => 'AIzaSyActwjFJ-s_UW__lou2tNDDgL3dmxFqREI',
    'offline' => false,
    'website' => 1,
    'hotel' => 183,
    'company' => 208,
    'db' => [
        [
            'host' => 'localhost',
            'name' => 'hotelpaq',
            'user' => 'tyrion',
            'pass' => 'AqJWyOC2FDQkWj1gvA6LajJbZxFSGZ',
		],
		[
            'host' => 'localhost',
            'name' => 'hotelpaq_logs',
            'user' => 'tyrion',
            'pass' => 'AqJWyOC2FDQkWj1gvA6LajJbZxFSGZ',
        ],
    ],
    'servers' => [],
    'smtp' => [
        'host' => 'mail.wt.com.mx',
        'pass' => 'noreply21',
        'from' => [
            'Contacto' => 'noreply@wt.com.mx',
        ],
    ],
    'routing' => [
        'setlog' => ['controller' => 'Website', 'action' => 'setLog'],
        'access/login' => ['controller' => 'Access', 'action' => 'login'],
        'access/logout' => ['controller' => 'Access', 'action' => 'logout'],
        'access/google' => ['controller' => 'Access', 'action' => 'loginGP'],
        'access/facebook' => ['controller' => 'Access', 'action' => 'loginFB'],
        'access/twitter' => ['controller' => 'Access', 'action' => 'loginTW'],
        'access/recover' => ['controller' => 'Access', 'action' => 'recover'],
        'access/restore' => ['controller' => 'Access', 'action' => 'restore'],
        'access/create' => ['controller' => 'Access', 'action' => 'create'],
        'access/confirm' => ['controller' => 'Access', 'action' => 'confirm'],
        'access/cancelrestore' => ['controller' => 'Access', 'action' => 'cancelrestore'],
        'account/update' => ['controller' => 'Access', 'action' => 'update'],
        'account/passupdate' => ['controller' => 'Access', 'action' => 'updatepass'],
        'reserva/create' => ['controller' => 'Reserva', 'action' => 'create'],
        'sitemap' => ['controller' => 'Sitemap', 'action' => 'index'],
    ],
    'request_rules' => [
        '^/c(\d+)' => '/category.php?id=$1',
        '^/a(\d+)' => '/article.php?id=$1',
        '^/promotions/(\d+)' => '/promotions.php?id=$1',
        '^/(terminos-condiciones|privacidad|hotel).*' => '/content.php?alias=$1',
        '^/widget2?.php' => '/widget_responsive.php',
    ],
];
