<?php
$demo = $factory->Demo;
?>
<!doctype html>
<html lang="en">
  <head>
    <title><?=$demo->name?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>css/theme.min.css">
  </head>
  <body>
      <h1>Bienvenido a <?=$demo->name?></h1>
    
    <script src="<?=TEMPLATE_PATH?>node_modules/jquery/dist/jquery.slim.min.js"></script>
    <script src="<?=TEMPLATE_PATH?>node_modules/popper.js/dist/popper.min.js"></script>
    <script src="<?=TEMPLATE_PATH?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>