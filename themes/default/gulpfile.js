let gulp = require('gulp');
let browserSync = require('browser-sync').create();
let sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let sourcemaps = require('gulp-sourcemaps');
let rename = require('gulp-rename');

gulp.task('bsproxy', function() {
  browserSync.init({
    proxy: "http://be-multi.test:8080/",
    files: ["*.php", "*.html", "*.js"],
    open: false
  });

  gulp.watch("*.scss", ['sass']);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  console.log("inicia tarea sass");
  return gulp.src("./scss/theme.scss")
    .pipe(sass())
    .pipe(sourcemaps.init())
    .pipe(cleanCSS(), function(details) {
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    })
    .pipe(sourcemaps.write())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest("./css"))
    .pipe(browserSync.stream());
});

gulp.task('default', ['sass','bsproxy']);