<!doctype html>
<html lang="en">
  <head>
    <title>Página no encontrada</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?=TEMPLATE_PATH?>css/theme.min.css">
  </head>
  <body>
	<div id="block_error">
		<div>
		 	<h2>Página no encontrada</h2>
			<p>
				El contenido que estás buscando no se encuentra
			</p>
		</div>
	</div>
    <script src="<?=TEMPLATE_PATH?>node_modules/jquery/dist/jquery.slim.min.js"></script>
    <script src="<?=TEMPLATE_PATH?>node_modules/popper.js/dist/popper.min.js"></script>
    <script src="<?=TEMPLATE_PATH?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>