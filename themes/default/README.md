# Nuevo frontend con yarn, sass, fontawesome, bootstrap


## Primer paso

Para saber si ya está instalado
```sh
yarn --version
```

Cuando no está instalado
```sh
brew install yarn
```

Cuando ya esta pero para asegurarse que esté actual
```sh
brew upgrade yarn
```


## Para iniciar el proyecto

Las reglas básicas
```sh
yarn init -yp
yarn add jquery popper.js bootstrap
yarn add browser-sync gulp gulp-clean-css gulp-rename gulp-sass gulp-sourcemaps --dev
```


## Configuración base

El archivo package.json debe no estar licenciado y debe estar especificado como privado, ejemplo:
```json
{
  "name": "default",
  "version": "1.0.0",
  "description": "Plantilla base default",
  "license": "UNLICENSED",
  "private": true,
  "dependencies": {
    "bootstrap": "^4.1.3",
    "jquery": "^3.3.1",
    "jquery-ui": "^1.12.1",
    "popper.js": "^1.14.4"
  },
  "devDependencies": {
    "browser-sync": "^2.24.7",
    "gulp": "^3.9.1",
    "gulp-clean-css": "^3.10.0",
    "gulp-rename": "^1.4.0",
    "gulp-sass": "^4.0.1",
    "gulp-sourcemaps": "^2.6.4"
  }
}
```

Los CSS deben generarse a partir de SASS, por lo que debe haber una carpeta para los css (ya compilados) y para los scss, para estos últimos solo debe haber un archivo en la carpeta, los adicionales deben estar en subcarpetas.

El archivo principal SASS debe ser *theme.scss*.

```sh
default/
    /css/
    /scss/
         /theme.scss
         /sections/
                  /home.scss
```

En el archivo *theme.scss* se debe importar los archivos de los paquetes externos y los locales de la plantilla que se está construyendo, por ejemplo:

```js
// importando Bootstrap 
@import "../node_modules/bootstrap/scss/bootstrap"; 

@import "../node_modules/jquery-ui/themes/base/datepicker.css"; 

@import "sections/home"; 
```



## Gulp, SASS

Para compilar los archivos SASS se utiliza una tarea en Gulp y se facilita su armado con BrowserSync, más info de los mismos en https://browsersync.io/docs/gulp, https://github.com/gulpjs/gulp/blob/v3.9.1/docs/API.md, https://gulpjs.com/

Ejemplo del archivo gulpfile.js:

```js
let gulp = require('gulp');
let browserSync = require('browser-sync').create();
let sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let sourcemaps = require('gulp-sourcemaps');
let rename = require('gulp-rename');

gulp.task('bsproxy', function() {
  browserSync.init({
    proxy: "http://localhost.test:8080/themes/default/index.html",
    files: ["*.php", "*.html", "*.js"],
    open: false
  });

  gulp.watch("*.scss", ['sass']);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  console.log("inicia tarea sass");
  return gulp.src("./scss/theme.scss")
    .pipe(sass())
    .pipe(sourcemaps.init())
    .pipe(cleanCSS(), function(details) {
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    })
    .pipe(sourcemaps.write())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest("./css"))
    .pipe(browserSync.stream());
});

gulp.task('default', ['sass','bsproxy']);
```

Para su ejecución solo se necesita correr el comando gulp en la misma carpeta raiz del proyecto, en donde también se encuentra el archivo gulpfile.js

```sh
gulp

# o con más logs
gulp -LLLL
```